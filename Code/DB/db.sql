-- MySQL dump 10.11
--
-- Host: localhost    Database: rpc_new
-- ------------------------------------------------------
-- Server version	5.0.95-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(45) default NULL,
  `password` varchar(128) default NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `billing_log`
--

DROP TABLE IF EXISTS `billing_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_log` (
  `id` int(11) NOT NULL auto_increment,
  `calling` varchar(16) default NULL,
  `called` varchar(16) default NULL,
  `start_time` datetime default NULL,
  `end_time` datetime default NULL,
  `duration` int(11) default NULL,
  `bill_duration` int(11) NOT NULL,
  `status` int(11) default NULL,
  `total_money` int(11) NOT NULL,
  `charging_time` datetime default NULL,
  `total_try_charging` int(11) NOT NULL,
  `cdr_id` int(11) NOT NULL,
  `msc` varchar(16) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=973836 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `call_temp`
--

DROP TABLE IF EXISTS `call_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `call_temp` (
  `id` int(11) NOT NULL auto_increment,
  `calling` varchar(16) default NULL,
  `called` varchar(16) default NULL,
  `called_money` int(11) default NULL,
  `max_duration` int(11) NOT NULL,
  `status` int(11) default '0' COMMENT '1. Accept, 2 Denie, 3. Not answer, 4.Not enought money, 99. Process',
  `msc` int(11) default '1',
  `call_time` datetime default NULL,
  `created_datetime` datetime NOT NULL,
  `start_call` datetime default NULL,
  `end_call` datetime default NULL,
  `bill_duration` int(11) default '0',
  `charging_status` int(11) default '0',
  PRIMARY KEY  (`id`),
  KEY `calling` (`calling`),
  KEY `calling_2` (`calling`),
  KEY `called` (`called`),
  KEY `created_datetime` (`created_datetime`),
  KEY `calling_3` (`calling`),
  KEY `called_2` (`called`),
  KEY `created_datetime_2` (`created_datetime`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=53020227 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `call_temp_2014_01_28`
--