<?php
//error_reporting(false);
require_once("lib/config.php");
require_once("lib/functions.php");
require_once("lib/sbMssqlADO.class.php");
require_once("lib/sbMysqlPDO.class.php");

$conn = NULL;

$conn = new sbMysqlPDO($server, $user, $password, $db);

//RENAME table call_temp
$sqlSelectMaxId = " SELECT max(id) as max_id FROM call_temp ";
$maxId = $conn->doSelectOne($sqlSelectMaxId);
$maxId = $maxId['max_id'];

$conn->doUpdate("rename table call_temp to call_temp_".date("Y_m_d",time()));

$sqlCreateTable = "
CREATE TABLE `call_temp` (
  `id` int(11) NOT NULL auto_increment,
  `calling` varchar(16) default NULL,
  `called` varchar(16) default NULL,
  `called_money` int(11) default NULL,
  `max_duration` int(11) NOT NULL,
  `status` int(11) default '0' COMMENT '1. Accept, 2 Denie, 3. Not answer, 4.Not enought money, 99. Process',
  `msc` int(11) default '1',
  `call_time` datetime default NULL,
  `created_datetime` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `calling` (`calling`),
  KEY `calling_2` (`calling`),
  KEY `called` (`called`),
  KEY `created_datetime` (`created_datetime`),
  KEY `calling_3` (`calling`),
  KEY `called_2` (`called`),
  KEY `created_datetime_2` (`created_datetime`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT={$maxId} DEFAULT CHARSET=latin1
";


$sqlCreateTable = "
CREATE TABLE `call_temp` (
  `id` int(11) NOT NULL auto_increment,
  `calling` varchar(16) default NULL,
  `called` varchar(16) default NULL,
  `called_money` int(11) default NULL,
  `max_duration` int(11) NOT NULL,
  `status` int(11) default '0' COMMENT '1. Accept, 2 Denie, 3. Not answer, 4.Not enought money, 99. Process',
  `msc` int(11) default '1',
  `call_time` datetime default NULL,
  `created_datetime` datetime NOT NULL,
  `start_call` datetime default NULL,
  `end_call` datetime default NULL,
  `bill_duration` int(11) default '0',
  `charging_status` int(11) default '0',
  PRIMARY KEY  (`id`),
  KEY `calling` (`calling`),
  KEY `calling_2` (`calling`),
  KEY `called` (`called`),
  KEY `created_datetime` (`created_datetime`),
  KEY `calling_3` (`calling`),
  KEY `called_2` (`called`),
  KEY `created_datetime_2` (`created_datetime`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT={$maxId} DEFAULT CHARSET=latin1
";

$conn->doUpdate($sqlCreateTable);


//CDR

$sqlSelectMaxId = " SELECT max(id) as max_id FROM call_temp ";
$maxId = $conn->doSelectOne($sqlSelectMaxId);
$maxId = $maxId['max_id'];

//RENAME table
$sqlSelectMaxId = " SELECT max(id) as max_id FROM cdr ";
$maxId = $conn->doSelectOne($sqlSelectMaxId);
$maxId = $maxId['max_id'];

$conn->doUpdate("rename table cdr to cdr_".date("Y_m_d",time()));

$sqlCreateTable = "
CREATE TABLE `cdr` (
  `id` int(11) NOT NULL auto_increment,
  `start` datetime NOT NULL default '0000-00-00 00:00:00',
  `callerid` varchar(80) NOT NULL default '',
  `src` varchar(80) NOT NULL default '',
  `dst` varchar(80) NOT NULL default '',
  `dcontext` varchar(80) NOT NULL default '',
  `channel` varchar(80) NOT NULL default '',
  `dstchannel` varchar(80) NOT NULL default '',
  `lastapp` varchar(80) NOT NULL default '',
  `lastdata` varchar(80) NOT NULL default '',
  `duration` int(11) NOT NULL default '0',
  `billsec` int(11) NOT NULL default '0',
  `disposition` varchar(45) NOT NULL default '',
  `amaflags` int(11) NOT NULL default '0',
  `accountcode` varchar(20) NOT NULL default '',
  `userfield` varchar(255) NOT NULL default '',
  `uniqueid` varchar(255) NOT NULL default '',
  `status` int(11) default '0',
  `inform_status` int(11) DEFAULT '0',
  PRIMARY KEY  (`id`),
  KEY `src` (`src`),
  KEY `channel` (`channel`),
  KEY `dcontext` (`dcontext`),
  KEY `start` (`start`),
  KEY `duration` (`duration`),
  KEY `src_2` (`src`),
  KEY `accountcode` (`accountcode`),
  KEY `userfield` (`userfield`)
) ENGINE=MyISAM AUTO_INCREMENT={$maxId} DEFAULT CHARSET=latin1
";

$conn->doUpdate($sqlCreateTable);


//DROP TABLE
$dateDrop = date("Y_m_d",time()-10*3600*24);

$sqlDropCallTemp = " DROP TABLE call_temp_{$dateDrop} ";
$conn->doUpdate($sqlDropCallTemp);

$sqlDropCdr = " DROP TABLE cdr_{$dateDrop} ";
$conn->doUpdate($sqlDropCdr);


?>
