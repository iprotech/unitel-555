<?php


function viewInfo($msisdn){
    global $_vasgateway;
    $postVar = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.bccsgw.viettel.com/">
                    <soapenv:Header/>
                    <soapenv:Body>
                       <web:gwOperation>
                          <Input>
                            <username>'.$_vasgateway['username'].'</username>
                            <password>'.$_vasgateway['password'].'</password>
                            <wscode>viewinfonew</wscode>
                            <!--1 or more repetitions:-->
                            <param name="GWORDER" value="1"/>
                            <param name="msisdn" value="'.$msisdn.'"/>
                          </Input>
                       </web:gwOperation>
                    </soapenv:Body>
                 </soapenv:Envelope>
               ';
    //sfContext::getInstance()->getLogger()->err("Charge:SOAPXML:".$postVar);
    //open connection
    $ch = curl_init();
    //set the url, number of POST vars, POST data
    $options = array(
            CURLOPT_URL	       => $_vasgateway['address'],
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects {$channelInfo['static_name']}
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $postVar,
            CURLOPT_HTTPHEADER	   => array('Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($postVar) ),
    );

    //open connection
    $ch = curl_init();
    curl_setopt_array( $ch, $options );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    $responeContent = curl_exec($ch);
    var_dump($responeContent);
    $result = array();
    if($err || !$responeContent)
    {
        $result[0] = 201;
    }
    else
    {
        //$responeContent = '77|The isdn does not exist';
        $subStr = substr($responeContent, strpos($responeContent, '<return>')+8);
        $responeContent = substr($subStr, 0, strpos($subStr, '</return>'));
        $result = explode('|', $responeContent);
    }
    return $result;
}


function charge($msisdn,$money,$shortcode,$feeType,$subType=1){
    global $_vasgateway;
    $postVar = '
                    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.bccsgw.viettel.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <web:gwOperation>
                              <Input>
                                <username>'.$_vasgateway['username'].'</username>
                                <password>'.$_vasgateway['password'].'</password>
                                <wscode>chargenew</wscode>
                                <!--1 or more repetitions:-->
                                <param name="GWORDER" value="1"/>
                                <param name="input" value="'.$msisdn.'|'.$shortcode.'|'.$feeType.'|'.$subType.'|'.$money.'|0"/>
                              </Input>
                           </web:gwOperation>
                        </soapenv:Body>
                     </soapenv:Envelope>
               ';
    //sfContext::getInstance()->getLogger()->err("Charge:SOAPXML:".$postVar);
    //open connection
    $ch = curl_init();
    //set the url, number of POST vars, POST data
    $options = array(
            CURLOPT_URL	       => $_vasgateway['address'],
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects {$channelInfo['static_name']}
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $postVar,
            CURLOPT_HTTPHEADER	   => array('Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($postVar) ),
    );

    //open connection
    $ch = curl_init();
    curl_setopt_array( $ch, $options );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    $responeContent = curl_exec($ch);
    var_dump($responeContent);
    $result = 0;
    if($err || !$responeContent)
    {
        $result[0] = 201;
    }
    else
    {
        //$responeContent = '77|The isdn does not exist';
        $subStr = substr($responeContent, strpos($responeContent, '<return>')+8);
        $responeContent = substr($subStr, 0, strpos($subStr, '</return>'));
        $result = explode('|',$responeContent);
    }
    return $result;
}

function convertDurationToMoney($duration){
    $block = 1;
    return round(ceil($duration/$block) * 800/60);
}

function convertMoneyToDuration($money){
   $block = 1;
   $moneyPerBlock = 800;
   return round((ceil($money/(800/60))-1)*$block);
}

function shortContent($str,$lenght){
    return substr($str,0,strripos(substr($str,0,$lenght)," "));
}

function explodeContent($content){
    $resutls = array();
    $contents = explode("#",$content);
    foreach($contents as $content){
        if(strlen($content)<=140){
            $results[] = $content;
        }else{
            while(1){
               $shortContent = shortContent($content,140);
               $results[] = $shortContent;
               $content = substr($content,strlen($shortContent)+1);
               if(strlen($content)<140){
                   $results[] = $content;
                   break;
               }
            }
        }
    }
    return $results;
}


function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

?>
