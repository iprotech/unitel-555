<?php
//error_reporting(false);
require_once("lib/config.php");
require_once("lib/functions.php");
require_once("lib/sbMysqlPDO.class.php");
require_once('mailer/class.phpmailer.php');
require_once('mailer/class.smtp.php');


$_msisdn = "2098868585";
$_email_account = "sinv@iprotech.vn"; 
$_email_password = "sinv@123";
//$_mail_list = array("thien@iprotech.vn","sivv@iprotech.vn");
$_mail_list = array("thien@iprotech.vn");
$errorMsg = array();
//MYSQL
$conn = new sbMysqlPDO($server, $user, $password, $db);
if(!$conn){
    $errorMsg[] = "Can not connect to MYSQL ";
}
// Check Asterisk
$statusAsterisk = exec('/etc/init.d/asterisk status');
$strAs = "is running";
if (!strpos($statusAsterisk,$strAs)) {
    $errorMsg[] = "Asterisk was stopped or not working ";
}

//Ping MSC
$ping = exec('ping -c 3 10.20.3.1'); //10.78.17.17
if(empty($ping)){
    $errorMsg[] = "Can not ping to MSC 10.20.3.1";
}
$ping = exec('ping -c 3 10.20.3.17'); //10.78.17.17
if(empty($ping)){
    $errorMsg[] = "Can not ping to MSC 10.20.3.17";
}
$ping = exec('ping -c 3 10.20.5.1'); //10.78.17.17
if(empty($ping)){
    $errorMsg[] = "Can not ping to MSC 10.20.5.1";
}
$ping = exec('ping -c 3 10.20.5.17'); //10.78.17.17
if(empty($ping)){
    $errorMsg[] = "Can not ping to MSC 10.20.5.17";
}

//SMSC
$ping = exec('ping -c 3 10.78.7.165'); //10.78.17.17
if(empty($ping)){
    $errorMsg[] = "Can not ping to SMSC 10.78.7.165";
}

//CHARING GW
$ping = exec('ping -c 3 10.78.17.17'); //10.78.17.17
if(empty($ping)){
    $errorMsg[] = "Can not ping to CHARGING GW 10.78.17.17";
}


// Check Info
$info = viewInfo($_msisdn);
if($info[3] != $_msisdn){
    $errorMsg[] = "Can not get phone number information (viewInfo) {$info[1]} ";
}

//Check charging
echo $sqlCheckToday = " SELECT sum(total_money) as total FROM billing_log WHERE charging_time >date(now()) ";
$rToday = $conn->doSelectOne($sqlCheckToday);

$timeCheck = date("Y-m-d H:i:s",time()-24*3600);
echo $sqlCheckYesterday = " SELECT sum(total_money) as total FROM billing_log WHERE charging_time>=date('$timeCheck') AND charging_time<='$timeCheck' ";
$rYesterday = $conn->doSelectOne($sqlCheckYesterday);

if($rToday['total']*2<$rYesterday['total']){
    $errorMsg[] = "Have problem on charging process, today: {$rToday['total']}, yesterday: {$rYesterday['total']}  ";
}


//CDR
echo $sqlCheckToday = " SELECT sum(duration) as total FROM cdr WHERE start >date(now()) ";
$cToday = $conn->doSelectOne($sqlCheckToday);

$timeCheck = date("Y-m-d H:i:s",time()-24*3600);
echo $sqlCheckYesterday = " SELECT sum(duration) as total FROM cdr WHERE start>=date('$timeCheck') AND start<='$timeCheck' ";
$cYesterday = $conn->doSelectOne($sqlCheckYesterday);

if($cToday['total']*2<$cYesterday['total']){
    $errorMsg[] = "Have problem on cdr, today: {$cToday['total']}, yesterday: {$cYesterday['total']}  ";
}


//HDD
$hdd = explode("\n",shell_exec('df -h'));
$hdd = explode(" ",preg_replace('/\s+/', ' ', $hdd[2]));
if(intval($hdd[4])>80){
    $errorMsg[]= "HDD is full: {$hdd[4]}, available {$hdd[3]} ";
}

//CPU
$cpus = explode("\n",shell_exec('top -b -n3 | grep "Cpu(s)"'));
var_dump($cpus);
for($i=0;$i<(count($cpus)-1);$i++){
    $arCpu = explode(" ",preg_replace('/\s+/', ' ',$cpus[$i]));
    $totalCPU += intval($arCpu[1])+intval($arCpu[2]);
}
$totalCPU = intval($totalCPU/3);
if($totalCPU>70){
    $errorMsg[]= "CPU is hight: ".$totalCPU;
}

//RAM
$rams = explode("\n",shell_exec('top -b -n3 | grep "Mem"'));
var_dump($rams);
for($i=0;$i<(count($rams)-1);$i++){
    $arRam = explode(" ",preg_replace('/\s+/', ' ',$rams[$i]));
    $totalRamUse += intval($arRam[3])/intval($arRam[1]);
}
$totalRamUse = intval($totalRamUse/3);
if($totalRamUse>70){
    $errorMsg[]= "RAM is hight: ".$totalRamUse;
}


//Report at 08h and 17h daily
if(date("H",time())=="08"||date("H",time())=="17"){
    if(date("i",time())>=0 and date("i",time())<=15){
        if(!count($errorMsg)) $subject = "1956 UNITEL REPORTING ";
        $errorMsg[]="System is working fine."
                   ."<br/> Revenue: Today = {$rToday['total']}KIP, Yesterday = {$rYesterday['total']}KIP "
                   ."<br/> HDD: {$hdd[4]}, available {$hdd[3]} "
                   ."<br/> CDR: today = {$cToday['total']}s, yesterday = {$cYesterday['total']}s "
                   ."<br/> CPU: $totalCPU%"
                   ."<br/> RAM: $totalRamUse%";
    }
}



var_dump($errorMsg);

if(count($errorMsg)){
    foreach($errorMsg as $msg){
        $sendContent .= "$msg <br/>";
    }
    $mail = new PHPMailer(true);                //New instance, with exceptions enabled
    $mail->IsSMTP();                            // tell the class to use SMTP
    $mail->SMTPAuth   = true;                   // enable SMTP authentication
    $mail->Port       = 465;                     // set the SMTP server port
    $mail->Host       = "smtp.gmail.com";       // SMTP server
    $mail->SMTPSecure = 'ssl'; 
    $mail->Username   = $_email_account;     // SMTP server username
    $mail->Password   = $_email_password;           // SMTP server password

    $mail->From       = $_email_account;
    $mail->FromName   = "1956 Unitel";
	
    foreach($_mail_list as $to){
        $mail->AddAddress($to);
    }
    if($subject) $mail->Subject  = $subject;
    else $mail->Subject  = "1956 UNITEL ERROR";
    $mail->MsgHTML($sendContent);
    $mail->IsHTML(true);
    $mail->Send();
}
?>