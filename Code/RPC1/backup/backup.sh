#!/bin/bash
# Set a value that we can use for a datestamp
# Our Base backup directory
BASEBACKUP="/backup/daily"

for DATABASE in `cat /backup/db-list.txt`
do
        # This is where we throw our backups.
        FILEDIR="$BASEBACKUP/$DATABASE"

        # Test to see if our backup directory exists.
        # If not, create it.
        if [ ! -d $FILEDIR ]
        then
                mkdir -p $FILEDIR
        fi

        echo -n "Exporting database:  $DATABASE"
        mysqldump --host=localhost --user=root --opt $DATABASE | gzip -c -9 > $FILEDIR/$DATABASE-`date +%Y-%m-%d`.sql.gz
        echo "      ......[ Done Exporting to local backup, now exporting for remote backup] "
        #cp $FILEDIR/$DATABASE-$DATE.sql.gz /backup/uploads/$DATABASE.sql.gz
        echo "      .......[Done]"
done
/etc/init.d/mysqld restart
ftp -inv 103.1.209.39 << EOF
user vasbackup backup@2014
put $FILEDIR/$DATABASE-`date +%Y-%m-%d`.sql.gz  55501$DATABASE-`date +%Y-%m-%d`.sql.gz
ls


# AutoPrune our backups.  This will find all files
# that are "MaxFileAge" days old and delete them.
#MaxFileAge=4
#find $BASEBACKUP -name '*.gz' -type f -mtime +$MaxFileAge -exec rm -f {} \;

