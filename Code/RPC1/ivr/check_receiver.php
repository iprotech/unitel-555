#!/usr/bin/php -q
<?php
    set_time_limit(30);
    require('lib/phpagi.php');
    require('lib/sbMysqlPDO.class.php');
    require('lib/functions.php');
    require('lib/config.php');
    error_reporting(false);
    $agi = new AGI();
    $conn = new sbMysqlPDO($server, $username, $password, $db);
    
    $callID = $agi->get_variable("call_id",true);
    $sqlCheck = " SELECT * FROM call_temp WHERE id = '{$callID}' ORDER BY id DESC LIMIT 1 ";
    $data = $conn->doSelectOne($sqlCheck);
    

    $sqlCheckTotal = "
                                                    SELECT count(*) as total
                                                    FROM call_temp WHERE called = '{$data['called']}'
                                                    AND date(created_datetime)=date(now()) AND status!=1 AND status!=0
                                     ";
    $totalCall = $conn->doSelectOne($sqlCheckTotal);
    if($totalCall['total']>3){
       $sqlUpdate = "UPDATE call_temp set status=3 WHERE id='{$callID}' ";
       $conn->doUpdate($sqlUpdate);
       $agi->stream_file($_voice_common_path."receiver_deny");
       $agi->exec_goto("rpc-555","555",1);
       exit();
    }
    
    
    $callID = $agi->get_variable("call_id",true);
    $called = $agi->get_variable("called",true);
    $calling = $agi->get_variable("calling",true);
    
    $start_time = date("Y-m-d H:i:s",time());
    $calledInfo = viewInfo($called);
    //$calledInfo[9] = 1000;
    $maxDuration = convertMoneyToDuration($calledInfo[9]);
    //$agi->say_digits($maxDuration);
    //$agi->say_digits(000);
    $sqlUpdate = "UPDATE call_temp set called_money='{$calledInfo[9]}',max_duration='{$maxDuration}' WHERE id='{$callID}' ";
    $conn->doUpdate($sqlUpdate);
    
    if(!$maxDuration){
       $sqlUpdate = "UPDATE call_temp set status=4 WHERE id='{$callID}' ";
       $conn->doUpdate($sqlUpdate);
       $agi->stream_file($_voice_common_path."receiver_not_money");
       $agi->exec_goto("rpc-555","555",1);
       exit();
    }else if($maxDuration>300){
        $maxDuration = 300;
    }else if($maxDuration<=10){
        $maxDuration=0;
        $sqlUpdate = "UPDATE call_temp set status=4 WHERE id='{$callID}' ";
        $conn->doUpdate($sqlUpdate);
        $agi->stream_file($_voice_common_path."receiver_not_money");
        $agi->exec_goto("rpc-555","555",1);
        exit();
    }
    
    $agi->set_variable("max_duration",$maxDuration);
    $agi->set_variable("calling",$data['calling']);
    $agi->set_variable("called",$data['called']);
    $outgoing = $_outgoing_way[$data['id']%2];
    $agi->set_variable("msc","msc1");
    $agi->exec_goto("rpc-connect","555",1);
?>
