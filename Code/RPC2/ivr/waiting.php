#!/usr/bin/php -q
<?php
  set_time_limit(30);
  require('lib/phpagi.php');
  require('lib/sbMysqlPDO.class.php');
  require('lib/functions.php');
  require('lib/config.php');
  error_reporting(0);
  $agi = new AGI();
  $conn = new sbMysqlPDO($server, $username, $password, $db);
  $msisdn = $agi->get_variable("CALLERID(num)");
  $msisdn = $msisdn['data'];
  $agi->set_variable("msisdn",$msisdn);
  $time = date('Y-m-d H:i:s',time());
  $start_micro = microtime_float();
  $callID = $agi->get_variable("call_id");
  $callID = $callID['data'];
  $i = 0;
  $startWaitReceiver = time();
  while(1){
      $sqlCheck = " SELECT * FROM call_temp WHERE id = '{$callID}' ORDER BY id DESC LIMIT 1 ";
      $data = $conn->doSelectOne($sqlCheck);
      if($data){
          //CHECK cac truong hop
          if($data['status']==0||$data['status']==99){
              $agi->stream_file($_voice_common_path."theme_waiting");
              $i++;
          }

          if($data['status']==1){
              //$agi->stream_file($_voice_common_path."receiver_accept");
              $start_time = date("Y-m-d H:i:s",time());
              $calledInfo = viewInfo($data['called']);
              $maxDuration = convertMoneyToDuration($calledInfo[9]);
              if(!$maxDuration){
                 $agi->stream_file($_voice_common_path."receiver_not_money");
                 $agi->exec_goto("rpc-555","555",1);
                 exit();
              }else if($maxDuration>300){
                  $maxDuration = 300;
              }else if($maxDuration<=10){
		    $maxDuration=0;
                  $agi->stream_file($_voice_common_path."receiver_not_money");
                  $agi->exec_goto("rpc-555","555",1);
                  exit();
              }
              $sqlUpdate = "UPDATE call_temp set called_money='{$calledInfo[9]}',max_duration='{$maxDuration}' WHERE id='{$callID}' ";
              $conn->doUpdate($sqlUpdate);
              $agi->set_variable("max_duration",$maxDuration);
              $agi->set_variable("calling",$data['calling']);
              $agi->set_variable("called",$data['called']);
              $outgoing = $_outgoing_way[$data['id']%2];
              $agi->set_variable("msc","msc1");
              $agi->exec_goto("rpc-connect-555","555",1);
              exit();
          }else if($data['status']==2){
              $agi->stream_file($_voice_common_path."receiver_deny");
              $agi->exec_goto("rpc-555","555",1);
              exit();
          }else if($data['status']==3){
              $agi->stream_file($_voice_common_path."receiver_not_answer");
              $agi->exec_goto("rpc-555","555",1);
              exit();
          }else if($data['status']==4){
              $agi->stream_file($_voice_common_path."receiver_not_money");
              $agi->exec_goto("rpc-555","555",1);
              exit();
          }else if($data['status']==99){
              if($startWaitReceiver<(time()-60)){
                  $agi->stream_file($_voice_common_path."receiver_not_answer");
                  $agi->exec_goto("rpc-555","555",1);
                  exit();
              }else{
                  $agi->stream_file($_voice_common_path."theme_waiting");
                  $i++;
              }
          }
          if($i++>6){
             $agi->stream_file($_voice_common_path."receiver_not_answer");
             $agi->exec_goto("rpc-555","555",1);
             exit();
          }
      }else{
          $agi->stream_file($_voice_common_path."receiver_not_answer");
          $agi->exec_goto("rpc-555","555",1);
          exit();
          break;
      }
  }
  
?>
